use kvs::KvStore;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name="KayVS")]
enum KVS {
    Get {
        key: String,
    },
    Set {
        key: String,
        value: String,
    },
    RM {
        key: String
    }
}

fn main() {
    let mut store: KvStore = Default::default();
    match KVS::from_args() {
        KVS::Get {key} => {
            let val = store.get(key).expect("no matching value found");
            println!("{}", val);
            //panic!("unimplemented");
        },
        KVS::Set {key, value} => {
            store.set(key, value);
            //panic!("unimplemented");
        },
        KVS::RM {key} => {
            store.remove(key);
            //panic!("unimplemented");
        }
    }
}
