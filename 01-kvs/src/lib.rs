use std::collections::HashMap;

pub struct KvStore {
    items: HashMap<String, String>,
}

impl Default for KvStore {
    fn default() -> Self {
        Self::new()
    }
}

impl KvStore {
    pub fn new() -> Self {
        KvStore {
            items: HashMap::new(),
        }
    }

    pub fn set(&mut self, key: String, value: String) {
        self.items.insert(key, value);
    }

    pub fn get(&self, key: String) -> Option<String> {
        let val = self.items.get(key.as_str());
        match val {
            Some(v) => Some(v.to_string()),
            _ => None,
        }
    }

    pub fn remove(&mut self, key: String) {
        self.items.remove(key.as_str());
    }
}
